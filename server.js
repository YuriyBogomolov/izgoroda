var env = process.env.NODE_ENV || 'development';
var config = require('./config/config')[env];
var express = require('express');
var http = require('http');
var passport = require('passport');
var mongoose = require('mongoose');
var fs = require('fs');
require('express-namespace');

// Actual configuration:

// Bootstrap models:
fs.readdirSync(config.root + '/models').forEach(function (file) {
  if (~file.indexOf('.js')) {
    require(config.root + '/models/' + file);
  }
});

// Express:
var app = express();
require('./config/mongoose')(app, mongoose);
require('./config/passport')(passport);
require('./config/express')(app, passport);
require('./config/routes')(app, passport);

// Start the app by listening on <port>
var port = process.env.PORT || 3000;
http.createServer(app).listen(port, function () {
  console.log('Express server listening on port ' + port);
});
