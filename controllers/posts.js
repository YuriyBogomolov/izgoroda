var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var User = mongoose.model('User');
var Comment = mongoose.model('Comment');
var cyrillic = require('transliteration.cyr');
var extend = require('util')._extend; // needed to merge modified post with the original one
var security = require('../tools/securityUtil');
var sanitizer = require('sanitizer');
var mail = require('../tools/mailUtil');

exports.load = function (req, res, next, permalink) {
  Post.load(permalink, function(err, post) {
    if (err)
      return next(err);

    if (!post)
      return next(new Error('Статья не найдена'));

    req.post = post;
    return next();
  });
};

exports.index = function(req, res){
  var criteria = {
    tags: 'новость'
  };

  if (!security.hasSpecialAccess(req.user)) {
    criteria.is_special = false;
  }

  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;
  var perPage = 30;
  var options = {
    perPage: perPage,
    page: page,
    criteria: criteria
  };

  Post.list(options, function(err, posts) {
    if (err)
      return res.render('500');

    Post.count(criteria).exec(function (err, count) {
      Post.distinct('tags', function (err, distinctTags){
        Post.find(criteria).sort('-date_published').limit(5).exec(function(err, threePosts) {
          if (err)
            return res.render('500');

          Comment.find({}, {"author": 1, "body": 1, "date": 1, "permalink": 1})
                 .sort({"date":-1})
                 .limit(5)
                 .populate('author', 'username')
                 .exec(function (err, recentComments){
            if (err)
              return res.render('500');

            return res.render('posts/index', {
              title: 'Блог Клуба «КудЫ» | Записи',
              posts: posts,
              page: page + 1,
              pages: Math.ceil(count / perPage),
              threeRecentPosts: threePosts,
              user: req.user,
              tagcloud: distinctTags,
              recent_comments: recentComments
            });
          });
        });
      });
    });
  });
};

exports.new = function(req, res) {
  res.render('posts/new', {
    title: 'Блог Клуба «КудЫ» | Новая запись',
    post: new Post({}),
    user: req.user
  });
};

exports.create = function(req, res) {
  var post = new Post(req.body);
  post.author = req.user;
  post.date_published = new Date();

  // some magic: http://stackoverflow.com/questions/2519818/create-a-permalink-with-javascript
  post.permalink = cyrillic.transliterate(post.title).replace(/[^a-z0-9]+/gi, '-').replace(/^-*|-*$/g, '').toLowerCase();
  post.permalink += '-' + post.date_published.getTime();
  post.tags = post.tags[0].split(/[,,]/);
  post.is_special = req.body.is_special != undefined;

  post.save(function (err, post) {
    if (!err) {
      req.flash('success', 'Запись сохранена!');

      mail.sendMessageToAllAdmins("Новая запись в блоге: \"" + post.title + "\"", mail.formatNewPostHtml(post));

      return res.redirect('/posts/'+post.permalink);
    }
    return res.render('posts/new', {
      title: 'Новая запись',
      post: post,
      user: req.user
    });
  });
};

exports.edit = function(req, res) {
  res.render('posts/edit', {
    title: 'Блог Клуба «КудЫ» | Редактирование записи «' + req.post.title + '»',
    post: req.post,
    user: req.user
  });
};

exports.update = function(req, res) {
  console.log(req.body);
  var post = req.post;
  post = extend(post, req.body);
  post.date_published = req.post.date_published; // we need to maintain original date!
  post.tags = post.tags[0].split(/[,,]/);
  post.is_special = req.body.is_special != undefined;

  post.save(function(err, savedPost) {
    if (!err) {
      return res.redirect('/posts/' + savedPost.permalink);
    }

    return res.render('posts/edit', {
      title: 'Блог Клуба «КудЫ» | Редактирование записи «' + req.post.title + '»',
      post: savedPost,
      errors: err,
      user: req.user
    });
  });
};

exports.show = function(req, res) {
  var user = req.user;

  if (security.canReadPost(user, req.post)) {
    Comment.populate(req.post.comments, { path: 'author', select: 'username avatar_url', model: 'User' }, function (err, populated){
      res.render('posts/show', {
        title: req.post.title,
        post: req.post,
        isAuthor: security.canModifyPost(user, req.post),
        user: req.user
      });
    });
  } else {
    req.flash('error', 'У вас недостаточно прав для просмотра этой записи.');
    res.redirect('/login');
  }
};

exports.destroy = function(req, res) {
  var post = req.post;
  post.remove(function(err){
    req.flash('info', 'Запись успешно удалена.');
    mail.sendMessageToAllAdmins("Запись \"" + post.title +"\" была удалена.",
        "<p>Имя удалившего пользователя: " + req.user.username + "</p>");
    res.redirect('/');
  });
};

exports.search = function(req, res) {
  var query = req.param('query');
  query = sanitizer.escape(query);
  query = ".*" + query + ".*";

  var criteria = {
    body: new RegExp(query, "im"),
    is_special: security.hasSpecialAccess(req.user)
  };
  var perPage = 30;
  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;
  var options = {
    perPage: perPage,
    page: page,
    criteria: criteria
  };

  Post.list(options, function(err, posts) {
    if (err)
      return res.render('500');

    Post.count(criteria)
        .exec(function (err, count) {
      res.render('search', {
        searchResults: posts,
        query: req.param('query'),
        page: page + 1,
        pages: Math.ceil(count / perPage),
        user: req.user
      });
    });
  });
};