var mongoose = require('mongoose');
var Deal = mongoose.model('Deal');
var ObjectId = mongoose.Schema.ObjectId;
var extend = require('util')._extend; // needed to merge modified post with the original one

exports.index = function (req, res) {
  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;
  var perPage = 30;
  var options = {
    perPage: perPage,
    page: page
  };

  Deal.list(options, function(err, deals) {
    if (err)
      return res.render('500');

    return res.render('business/index', { deals: deals, user: req.user });
  });
};

exports.new = function (req, res) {
  return res.render('business/new', { deal: new Deal(), user: req.user });
};

exports.create = function (req, res) {
  var deal = new Deal(req.body);
  deal.date_published = (new Date()).getTime();

  deal.save(function(err, deal) {
    if (!err) {
      req.flash('success', 'Предложение сохранено!');
      return res.redirect('/business');
    }
    console.log(err);
    return res.render('business/new', {
      deal: deal,
      user: req.user
    });
  });
};

exports.edit = function (req, res) {
  var title = req.params.title;

  Deal.findOne({ title: title }, function (err, deal) {
    res.render('business/edit', {
      deal: deal,
      user: req.user
    });
  });
};

exports.update = function (req, res) {
  var title = req.params.title;
  var deal =

  Deal.findOne({ title: title }, function (err, deal) {
    var date = deal.date_published;
    deal = extend(deal, req.body);
    deal.date_published = date; // we need to maintain them

    deal.save(function(err) {
      if (!err) {
        return res.redirect('/business');
      }

      return res.render('business/edit', {
        deal: deal,
        errors: err,
        user: req.user
      });
    });
  });
};

exports.destroy = function(req, res) {
  Deal.findByIdAndRemove(req.params.id, function(err){
    req.flash('info', 'Запись успешно удалена.');
    res.redirect('/business');
  });
};