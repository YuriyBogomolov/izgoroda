var mongoose = require('mongoose');
var Comment = mongoose.model('Comment');
var cyrillic = require('transliteration.cyr');
var extend = require('util')._extend; // needed to merge modified post with the original one
var security = require('../tools/securityUtil');
var mail = require('../tools/mailUtil');
var sanitizer = require('sanitizer');

exports.load = function (req, res, next, id) {
  var post = req.post;

  for (var i=0; i< post.comments.length; ++i) {
    if (post.comments[i].id === id) {
      req.comment = post.comments[i];
      return next();
    }
  }
}

/**
 * Create comment
 */

exports.create = function (req, res) {
  var post = req.post;
  var user = req.user;

  if (!req.body.body)
    return res.redirect('/posts/'+ post.permalink);

  post.addComment(user, req.body, function (err) {
    if (err) return res.render('500');

    mail.sendMessageToAllAdmins(
        "Новый комментарий к записи \"" + req.post.title + "\"",
        mail.formatNewCommentHtml(req.post, user, sanitizer.escape(req.body.body.replace(/\r\n/g, "<br/>"))));
    return res.redirect('/posts/'+ post.permalink);
  });
};

/**
 * Delete comment
 */

exports.destroy = function (req, res) {
  var post = req.post;

  var index = -1;
  for (var i=0; i< post.comments.length; ++i) {
    if (post.comments[i].id === req.params.commentId) {
      index = i;
      break;
    }
  }

  post.comments.splice(index, 1);
  post.save(function (err) {
    if (err) {
      req.flash('error', 'Oops! The comment was not found');
    } else {
      req.flash('info', 'Removed comment');
    }
    res.redirect('/posts/' + post.permalink);
  });
};
