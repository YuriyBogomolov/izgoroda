var fs = require('fs');
var env = process.env.NODE_ENV || 'development';
var config = require('../config/config')[env];
var mime = require('mime');
var thumb = require('node-thumbnail').thumb;
var mail = require('../tools/mailUtil');
var gm = require('gm');

var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file, index) {
      var curPath = path + "/" + file;
      if(fs.statSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};


exports.index = function (req, res) {
  fs.readdir(config.root + '/public/storage', function(err, files) {
    if (err) throw err;

    var dirs = [];

    files.forEach(function(file){
      var stat = fs.statSync(config.root + '/public/storage/' + file);
      if (stat.isDirectory()) {
        dirs.push(file);
      }
    });

    res.render('galleries/index', {
      user: req.user,
      files: dirs
    });
  });
};

exports.show = function (req, res) {
  var path = '/';
  var prevPath = '';
  var hideNewButton = false;

  if (req.params.name) {
    path += req.params.name + '/';
    prevPath += '/gallery';
  }
  if (req.params.place) {
    path += req.params.place + '/';
    prevPath += '/' + req.params.name;
  }
  if (req.params.part) {
    path += req.params.part + '/';
    prevPath += '/' + req.params.place;
    hideNewButton = true;
  }

  function showGallery(files) {
    var dirs = [];
    var images = [];
    if (files) {
      files.forEach(function (file) {
        var stat = fs.statSync(config.root + '/public/storage' + path + file);
        if (stat.isDirectory() && file != 'thumb') {
          dirs.push(file);
        } else {
          if (stat.isFile()) {
            var lookup = mime.lookup(file);
            switch (lookup) {
              case 'image/jpeg':
              case 'image/png':
              case 'image/bmp':
              case 'image/x-windows-bmp':
              case 'image/pjpeg':
              {
                images.push(file);
                break;
              }
              default:
                break;
            }
          }
        }
      });
    }

    res.render('galleries/show', {
      user: req.user,
      dirs: dirs,
      images: images,
      path: path,
      prevPath: prevPath,
      hideNewButton: hideNewButton
    });
  }

  fs.readdir(config.root + '/public/storage' + path, function(err, files) {
    if (files && files.indexOf('thumb') < 0) {
      fs.mkdirSync(config.root + '/public/storage' + path + '/thumb');
      thumb({
        source: config.root + '/public/storage' + path,
        destination: config.root + '/public/storage' + path + '/thumb',
        suffix: '',
        width: 200,
        concurrency: 4
      }, function() {
        showGallery(files);
      });
    } else {
      showGallery(files);
    }
  });
};

exports.new = function (req, res) {
  var path = '/';

  if (req.params.name) path += req.params.name + '/';
  if (req.params.place) path += req.params.place + '/';
  if (req.params.part) path += req.params.part + '/';

  res.render('galleries/new', {
    user: req.user,
    path: path
  });
};

exports.edit = function (req, res) {
  var path = '/';
  var title = '';

  if (req.params.name) {
    path += req.params.name + '/';
    title = req.params.name;
  }
  if (req.params.place) {
    path += req.params.place + '/';
    title = req.params.place;
  }
  if (req.params.part) {
    path += req.params.part + '/';
    title = req.params.part;
  }

  res.render('galleries/edit', {
    user: req.user,
    path: path,
    galleryTitle: title
  });
};

exports.handlePost = function (req, res) {
  var path = '/';
  var renamePath = '/';
  var prevPath = '/';
  var hideNewButton = false;

  if (req.params.name) {
    path += req.params.name + '/';
    renamePath += req.params.name;
  }
  if (req.params.place) {
    path += req.params.place + '/';
    renamePath += '/' + req.params.place;
    prevPath += req.params.name + '/';
  }
  if (req.params.part) {
    path += req.params.part + '/';
    renamePath += '/' + req.params.part;
    prevPath += req.params.place + '/';
  }

  var galleryName = req.body.title ? req.body.title.trim() : undefined;
  var editedTitle = req.body.editedtitle ? req.body.editedtitle.trim() : undefined;

  if (galleryName) {
    // this is postback from 'new gallery' page
    var newGalleryPath = config.root + '/public/storage' + path + galleryName;

    fs.mkdir(newGalleryPath, function (err) {
      if (err) throw err;
      fs.mkdirSync(newGalleryPath + '/thumb'); // folder for thumbnails
      mail.sendMessageToAllAdmins("Новая галерея \"" + galleryName + "\"", mail.formatNewGalleryHtml(galleryName));
      res.redirect('/gallery' + path + galleryName);
    });
  } else if (editedTitle) {
    // this is postback from 'Edit' page
    var editedGalleryPath = config.root + '/public/storage' + prevPath + editedTitle;
    var oldGalleryPath = config.root + '/public/storage' + renamePath;

    fs.rename(oldGalleryPath, editedGalleryPath, function (err) {
      if (err) throw err;
      res.redirect('/gallery' + prevPath + editedTitle);
    });
  } else {
    // this is postback from DropZone
    if (req.files.length > 0) {
      var file = req.files[0];
      var newPath = config.root + '/public/storage' + path + file.originalname;

      gm(file.path).autoOrient().write(newPath, function (err) {
        if (err) throw err;
        res.end("upload complete");
      });
    }
  }
};

exports.regenerate = function (req, res) {
  var path = '/';

  if (req.params.name) {
    path += req.params.name + '/';
  }
  if (req.params.place) {
    path += req.params.place + '/';
  }
  if (req.params.part) {
    path += req.params.part + '/';
  }

  thumb({
    source: config.root + '/public/storage' + path,
    destination: config.root + '/public/storage' + path + 'thumb',
    suffix: '',
    width: 200,
    concurrency: 4
  }, function() {
  });
  res.redirect('/gallery' + path);
};

exports.destroy = function (req, res) {
  var path = '/';
  var prevPath = '';

  if (req.params.name) {
    path += req.params.name + '/';
    prevPath += '/gallery';
  }
  if (req.params.place) {
    path += req.params.place + '/';
    prevPath += '/' + req.params.name;
  }
  if (req.params.part) {
    path += req.params.part + '/';
    prevPath += '/' + req.params.place;
  }

  deleteFolderRecursive(config.root + '/public/storage' + path);
  res.redirect(prevPath);
};

exports.destroyImage = function (req, res) {
  var path = '/';

  if (req.params.name) {
    path += req.params.name + '/';
  }
  if (req.params.place) {
    path += req.params.place + '/';
  }
  if (req.params.part) {
    path += req.params.part + '/';
  }

  var imageName = req.params.image;

  try {
    fs.unlink(config.root + '/public/storage' + path + imageName, function (err) {
      if (err) throw err;
      if (fs.existsSync(config.root + '/public/storage' + path + 'thumb/' + imageName)) {
        fs.unlinkSync(config.root + '/public/storage' + path + 'thumb/' + imageName);
      }
      res.redirect('/gallery' + path);
    });
  } catch(errorData) {
    console.log(errorData);
  }
};

exports.getList = function (req, res) {
  var jsonFiles = [];
  fs.readdir(config.root + '/public/storage', function (err, folders) {
    for (var index = 0; index<folders.length; ++index) {
      var name = folders[index];
      var stats = fs.lstatSync('./public/storage/' + name);

      if (stats.isDirectory()) {
        var files = fs.readdirSync('./public/storage/' + name);
        for (var i=0; i<files.length; ++i) {
          jsonFiles.push({ image: '/public/storage/' + name + '/' + files[i], folder: name });
        }
      }
    }

    res.json(jsonFiles);
  });
};