var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var security = require('../tools/securityUtil');

/**
 * List items tagged with a tag
 */

exports.index = function (req, res) {
  var criteria = {
    tags: new RegExp('^' + req.param('tag') + '$', 'ig')
  };

  if (!security.hasSpecialAccess(req.user)) {
    criteria.is_special = false;
  }

  var perPage = 30;
  var page = (req.param('page') > 0 ? req.param('page') : 1) - 1;
  var options = {
    perPage: perPage,
    page: page,
    criteria: criteria
  };

  Post.list(options, function(err, posts) {
    if (err)
      return res.render('500');

    Post.count(criteria)
        .exec(function (err, count) {
      res.render('search', {
        searchResults: posts,
        tag: req.param('tag'),
        page: page + 1,
        pages: Math.ceil(count / perPage)
      });
    });
  });
};
