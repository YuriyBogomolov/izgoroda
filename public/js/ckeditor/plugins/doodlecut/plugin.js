CKEDITOR.plugins.add('doodlecut', {
  init: function (editor) {
    CKEDITOR.addCss("cut {display:block; height: 2px;width: 100%;background: #DDDD99;}");
    CKEDITOR.dtd.cut = {};
    CKEDITOR.dtd.$block.cut = 1;
    CKEDITOR.dtd.body.cut = 1;

    editor.addCommand( 'insertPagecut', {
      exec: function (editor) {
        var element = CKEDITOR.dom.element.createFromHtml( '<cut></cut>' );
        element.unselectable();
        editor.insertElement(element);
      }
    });

    editor.ui.addButton('Pagecut', {
      label: 'Вставить кат',
      command: 'insertPagecut',
      icon: this.path + 'images/icon.png'
    });
  }
});