module.exports = {
  SecurityGroups: {
    admin: 'Administrator',
    writer: 'Writer',
    special: 'Special',
    commentator: 'Commentator',
    banned: 'Banned'
  },

  canReadPost: function (user, post) {
    if (!user) {
      return !post.is_special;
    }

    switch (user.security_group) {
      case this.SecurityGroups.banned:
        return false;
      default:
        return true;
    }
  },

  canModifyPost: function (user, post) {
    // anon:
    if (!user) {
      return false;
    }

    switch (user.security_group) {
      case this.SecurityGroups.admin:
        return true;
      case this.SecurityGroups.writer:
        return user._id.toString() === post.author._id.toString();
      case this.SecurityGroups.commentator:
      case this.SecurityGroups.special:
      default:
        return false;
    }
  },

  canWritePost: function (user) {
    // anon:
    if (!user) {
      return false;
    }

    switch (user.security_group) {
      case this.SecurityGroups.admin:
      case this.SecurityGroups.writer:
        return true;
      default:
        return false;
    }
  },

  canUploadFiles: function (user) {
    // anon:
    if (!user) {
      return false;
    }

    switch (user.security_group) {
      case this.SecurityGroups.admin:
      case this.SecurityGroups.writer:
        return true;
      default:
        return false;
    }
  },

  hasSpecialAccess: function (user) {
    if (!user)
      return false;

    switch (user.security_group) {
      case this.SecurityGroups.admin:
      case this.SecurityGroups.writer:
      case this.SecurityGroups.special:
        return true;
      default:
        return false;
    }
  },

  isAdmin: function (user) {
    if (!user)
      return false;

    switch (user.security_group) {
      case this.SecurityGroups.admin:
        return true;
      default:
        return false;
    }
  },

  canDeleteComment: function (user, commentUserId) {
    return this.canWritePost(user) || user._id.toString() === commentUserId.toString();
  }
};