var mailer = require('nodemailer');
var transport = mailer.createTransport("SMTP", {
  host: "smtp.beget.ru",
  auth: {
    user: "postmaster@izgoroda.com",
    pass: "PI3FKq3u"
  }
});
var mongoose = require('mongoose');
var User = mongoose.model('User');
var securityUtil = require('./securityUtil');

module.exports = {
  sendMessageToAllAdmins: function (subject, htmlmessage) {
    User.distinct('email', { security_group: securityUtil.SecurityGroups.admin }, function (err, emails) {
      if (err) throw err;

      var message = {
        from: "Почтальон Из Города <postmaster@izgoroda.com>",
        to: emails.join(','),
        subject: subject,
        generateTextFromHTML: true,
        html: htmlmessage,
        attachments: []
      };

      transport.sendMail(message);
    });
  },

  formatNewCommentHtml: function (post, user, body) {
    return "<p>Запись: www.izgoroda.com/posts/" + post.permalink + "</p><p>Пользователь: " +
        user.username + "</p><p>Комментарий: " + body + "</p>";
  },

  formatNewPostHtml: function (post) {
    return "<p>Запись: www.izgoroda.com/posts/" + post.permalink + "</p><p>Требует спецдоступа: " + post.is_special + "</p>";
  },

  formatNewGalleryHtml: function(galleryName) {
    return "<p>Ссылка на галерею: www.izgoroda.com/gallery/" + galleryName + "</p>"
  }
};