var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var userPlugin = require('../tools/plugins/mongoose-user');
var bcrypt   = require('bcrypt-nodejs');
var oAuthTypes = ['facebook', 'google', 'vkontakte', 'twitter'];

/**
 * User Schema
 */

var UserSchema = new Schema({
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  username: { type: String, default: '' },
  provider: { type: String, default: '' },
  hashed_password: { type: String, default: '' },
  salt: { type: String, default: '' },
  authToken: { type: String, default: '' },
  last_login: { type: Date, default: new Date() },
  is_online: { type: Boolean, default: false },
  avatar_url: { type: String, default: '' },
  security_group: { type: String, default: 'Commentator' },
  facebook: {},
  google: {},
  vkontakte: {},
  twitter: {}
});

/**
 * Virtuals
 */

UserSchema
    .virtual('password')
    .set(function(password) {
      this._password = password;
      this.salt = bcrypt.genSaltSync(8);
      this.hashed_password = this.encryptPassword(password);
    })
    .get(function() { return this._password });
/*
*//**
 * Validations
 *//*

var validatePresenceOf = function (value) {
  return value && value.length;
};*/

// the below 5 validations only apply if you are signing up traditionally
/*

UserSchema.path('email').validate(function (email) {
  if (this.doesNotRequireValidation())
    return true;
  return email.length;
}, 'Email не может быть пустым.');

UserSchema.path('email').validate(function (email, fn) {
  var User = mongoose.model('User');
  if (this.doesNotRequireValidation())
    fn(true);

  // Check only when it is a new user or when email field is modified
  if (this.isNew || this.isModified('email')) {
    User.find({ email: email }).exec(function (err, users) {
      fn(!err && users.length === 0)
    });
  } else fn(true);
}, 'Такой email уже зарегистрирован.');

UserSchema.path('hashed_password').validate(function (hashed_password) {
  if (this.doesNotRequireValidation())
    return true;
  return hashed_password.length;
}, 'Пароль не может быть пустым.');
*/


/**
 * Pre-save hook
 */

/*UserSchema.pre('save', function(next) {
  if (!this.isNew) return next();

  if (!validatePresenceOf(this.password)
      && !this.doesNotRequireValidation())
    return next(new Error('Invalid password'));
  else
    return next();
});*/

/**
 * Methods
 */
UserSchema.methods = {
  encryptPassword: function (password) {
    return bcrypt.hashSync(password, this.salt, null);
  },

  /**
   * Validation is not required if using OAuth
   */
  doesNotRequireValidation: function() {
    return ~oAuthTypes.indexOf(this.provider);
  },

  /**
   * Validates entered password
   */
  isValidPassword: function(password) {
    return this.encryptPassword(password) === this.hashed_password;
  },

  isBanned: function() {
    return this.security_group === 'Banned';
  }
};

module.exports = mongoose.model('User', UserSchema);