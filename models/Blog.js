var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var BlogSchema = new Schema({
  users: { type: [ObjectId], ref: 'User' },
  sections: { type: [Number], ref: 'Section' },
  metadata: String, // TODO FIXME
  settings: String // TODO FIXME
});

mongoose.model('Blog', BlogSchema);