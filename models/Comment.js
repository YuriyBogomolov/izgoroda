var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var CommentSchema = new Schema({
  author: { type: ObjectId, ref: 'User' },
  body: { type: String },
  date: { type : Date, default: new Date() },
  responses: [CommentSchema],
  users_upvoted: [{ type: ObjectId, ref: 'User' }],
  users_downvoted: [{ type: ObjectId, ref: 'User' }],
  rating: { type: Number, default: 0 },
  permalink: String
});

mongoose.model('Comment', CommentSchema);