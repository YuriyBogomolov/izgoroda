var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Comment = mongoose.model('Comment');
var sanitizer = require('sanitizer');

var PostSchema = new Schema({
  permalink: { type: String },
  title: { type: String, required: true },
  author: { type: ObjectId, required: true, ref: 'User' },
  body: { type: String, required: true },
  date_published: { type: Date, required: true },
  comments: [{ type: ObjectId, ref: 'Comment' }],
  tags: [String],
  is_special: { type: Boolean, default: false },
  image_url: String
});

PostSchema.statics = {
  list: function (options, callback) {
    var criteria = options.criteria || {};

    this.find(criteria)
        .populate('author', 'username avatar_url')
        .sort({'date_published': -1}) // sort by date
        .limit(options.perPage)
        .skip(options.perPage * options.page)
        .exec(callback);
  },

  load: function(permalink, callback) {
    this.findOne({ permalink: permalink })
        .populate('author', 'username avatar_url')
        .populate('comments', 'author body date')
        .populate('comments.author', 'username avatar_url')
        .exec(callback);
  }
};

PostSchema.methods = {
  addComment: function (user, comment, callback) {
    var newComment = new Comment();
    newComment.author = user._id;
    newComment.body = sanitizer.escape(comment.body);
    newComment.body = newComment.body.replace(/\r\n/g, "<br/>");
    newComment.date = new Date();
    newComment.permalink = this.permalink;

    var that = this;
    newComment.save(function(err, savedComment) {
      that.comments.push(savedComment._id);
      that.save(callback);
    });
  }
};

mongoose.model('Post', PostSchema);