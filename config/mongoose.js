var pkg = require('../package');
var env = process.env.NODE_ENV || 'development';
var config = require('./config')[env];

module.exports = function(app, mongoose) {
  mongoose.connect(config.db);
};