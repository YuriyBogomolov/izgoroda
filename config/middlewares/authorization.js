var security = require('../../tools/securityUtil');

exports.requiresLogin = function (req, res, next) {
  if (req.isAuthenticated())
    return next();

  if (req.method == 'GET')
    req.session.returnTo = req.originalUrl;

  return res.redirect('/login');
};

/*
 *  User authorization routing middleware
 */

exports.user = {
  hasAuthorization: function (req, res, next) {
    if (req.profile.id != req.user.id) {
      req.flash('info', 'You are not authorized');
      return res.redirect('/users/' + req.profile.id);
    }
    return next();
  }
};

/*
 *  Article authorization routing middleware
 */

exports.post = {
  hasAuthorization: function (req, res, next) {
    if (req.user && req.post && !security.canModifyPost(req.user, req.post)) {
      req.flash('info', 'You are not authorized');
      return res.redirect('/posts/' + req.post.permalink);
    }
    return next();
  },

  canWrite: function(req, res, next) {
    if (req.user && security.canWritePost(req.user)) {
      return next();
    } else {
      req.flash('info', 'Вы не имеете права писать статьи!');
      return res.redirect('/');
    }
  },

  hasNormalOrSpecialAccess: function (req, res, next) {
    if (req.post.is_special) {
      if (req.user && security.hasSpecialAccess(req.user)) {
        return next();
      } else {
        return res.redirect('/');
      }
    } else {
      if (req.user && req.user.isBanned())
        return res.redirect('/');
      return next();
    }
  }
};

/**
 * Comment authorization routing middleware
 */

exports.comment = {
  hasAuthorization: function (req, res, next) {
    // if the current user is comment owner or article owner
    // give them authority to delete
    if (req.user.id === req.comment.author.id ||
        req.user.id === req.post.author.id ||
        security.canDeleteComment(req.user, req.comment.author)) {
      next();
    } else {
      req.flash('info', 'You are not authorized');
      res.redirect('/posts/' + req.post.permalink);
    }
  }
};

exports.admin = {
  hasAuthorization: function (req, res, next) {
    if (security.isAdmin(req.user)) {
      next();
    } else {
      req.flash('error', 'У вас недостаточно полномочий для просмотра этой страницы');
      res.redirect('/');
    }
  }
};

exports.images = {
  hasAuthorization: function (req, res, next) {
    if (security.canUploadFiles(req.user)) {
      next();
    } else {
      req.flash('error', 'У вас недостаточно полномочий для просмотра этой страницы');
      res.redirect('/');
    }
  }
};