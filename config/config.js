var path = require('path');
var rootPath = path.resolve(__dirname + '../..');

module.exports = {
  development: {
    root: rootPath,
    db: 'mongodb://localhost:27017/izgoroda',
    auth: {
      'facebookAuth': {
        'clientID': '281081745253021', // your App ID
        'clientSecret': '049bf47c6bc3b4dfb8cf2e090223fe88', // your App Secret
        'callbackURL': 'http://localhost:3000/auth/facebook/callback'
      },

      'twitterAuth': {
        'consumerKey': 'your-consumer-key-here',
        'consumerSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:3000/auth/twitter/callback'
      },

      'googleAuth': {
        'clientID': 'your-secret-clientID-here',
        'clientSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:3000/auth/google/callback'
      },

      'vkontakteAuth': {
        'clientID': '4190382',
        'clientSecret': '2YxvC8nrK232EG1laUJr',
        'callbackURL': 'http://localhost:3000/auth/vkontakte/callback'
      }
    }
  },
  production: {
    root: rootPath,
    db: 'mongodb://localhost:27017/izgoroda',
    auth: {
      'facebookAuth': {
        'clientID': '281081745253021', // your App ID
        'clientSecret': '049bf47c6bc3b4dfb8cf2e090223fe88', // your App Secret
        'callbackURL': 'http://izgoroda.com/auth/facebook/callback'
      },

      'twitterAuth': {
        'consumerKey': 'your-consumer-key-here',
        'consumerSecret': 'your-client-secret-here',
        'callbackURL': 'http://izgoroda.com/auth/twitter/callback'
      },

      'googleAuth': {
        'clientID': 'your-secret-clientID-here',
        'clientSecret': 'your-client-secret-here',
        'callbackURL': 'http://izgoroda.com/auth/google/callback'
      },

      'vkontakteAuth': {
        'clientID': '4190382',
        'clientSecret': '2YxvC8nrK232EG1laUJr',
        'callbackURL': 'http://izgoroda.com/auth/vkontakte/callback'
      }
    }
  }
};