var auth = require('./middlewares/authorization');

// controllers:
var posts = require('../controllers/posts');
var tags = require('../controllers/tags');
var images = require('../controllers/images');
var business = require('../controllers/business');
var sections = require('../controllers/sections');
var comments = require('../controllers/comments');

var postEditAuth = [auth.requiresLogin, auth.post.hasAuthorization];
var postWriteAuth = [auth.requiresLogin, auth.post.canWrite];
var commentAuth = [auth.requiresLogin, auth.comment.hasAuthorization];
var adminAuth = [auth.requiresLogin, auth.admin.hasAuthorization];
var imageAuth = [auth.requiresLogin, auth.images.hasAuthorization];
var postSpecialAuth = [auth.post.hasNormalOrSpecialAccess];

module.exports = function(app, passport) {
  //===========================================================================
  // LOGIN/SIGNUP
  //===========================================================================
  // show the login form
  app.get('/login', function(req, res) {
    res.render('login', { message: req.flash('loginMessage') });
  });

  // process the login
  app.post('/login', passport.authenticate('local-login', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  }));

  // show the signup form
  app.get('/signup', function(req, res) {
    res.render('signup', { message: req.flash('signupMessage') });
  });

  // process the signup form
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/profile',
    failureRedirect: '/signup',
    failureFlash: true
  }));

  app.get('/profile', auth.requiresLogin, function(req, res) {
    res.render('profile', { user : req.user });
  });

  // logout
  app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
  });

  // AUTHORIZATION (LINKING):
  app.get('/connect/local', function(req, res) {
    res.render('connect-local', { message: req.flash('loginMessage') });
  });
  app.post('/connect/local', passport.authenticate('local-signup', {
    successRedirect : '/profile', // redirect to the secure profile section
    failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  }));

  // ==========================================================================
  // FACEBOOK
  // ==========================================================================
  app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
  app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    successRedirect : '/profile',
    failureRedirect : '/login'
  }));
  app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));
  app.get('/connect/facebook/callback', passport.authorize('facebook', {
    successRedirect : '/profile',
    failureRedirect : '/'
  }));

  // ==========================================================================
  // VKONTAKTE
  // ==========================================================================
  app.get('/auth/vkontakte', passport.authenticate('vkontakte', { scope : ['email'] }));
  app.get('/auth/vkontakte/callback', passport.authenticate('vkontakte', {
    successRedirect : '/profile',
    failureRedirect : '/login'
  }));
  app.get('/connect/vkontakte', passport.authorize('vkontakte', { scope : ['email'] }));
  app.get('/connect/vkontakte/callback', passport.authorize('vkontakte', {
    successRedirect : '/profile',
    failureRedirect : '/'
  }));

  // ==========================================================================
  // UNLINKING
  // ==========================================================================
  // local -----------------------------------
  app.get('/unlink/local', function(req, res) {
    var user            = req.user;
    user.email           = undefined;
    user.hashed_password = undefined;
    user.save(function(err) {
      res.redirect('/profile');
    });
  });

  // facebook -------------------------------
  app.get('/unlink/facebook', auth.requiresLogin, function(req, res) {
    var user            = req.user;
    user.facebook.token = undefined;
    user.markModified('facebook');
    user.save(function(err) {
      res.redirect('/profile');
    });
  });

  // vkontakte -------------------------------
  app.get('/unlink/vkontakte', function(req, res) {
    var user             = req.user;
    user.vkontakte.token = undefined;
    user.markModified('vkontakte');
    user.save(function(err) {
      if (err) throw err;
      res.redirect('/profile');
    });
  });

  // twitter --------------------------------
  app.get('/unlink/twitter', auth.requiresLogin, function(req, res) {
    var user           = req.user;
    user.twitter.token = undefined;
    user.save(function(err) {
      res.redirect('/profile');
    });
  });

  // google ---------------------------------
  app.get('/unlink/google', auth.requiresLogin, function(req, res) {
    var user          = req.user;
    user.google.token = undefined;
    user.save(function(err) {
      res.redirect('/profile');
    });
  });

  //===========================================================================
  // POSTS
  //===========================================================================
  app.param('permalink', posts.load);
  app.get('/posts', posts.index);
  app.post('/posts', auth.requiresLogin, posts.create);
  app.get('/posts/new', postWriteAuth, posts.new);
  app.get('/posts/:permalink', postSpecialAuth, posts.show);
  app.get('/posts/:permalink/edit', postEditAuth, posts.edit);
  app.put('/posts/:permalink', postEditAuth, posts.update);
  app.del('/posts/:permalink', postEditAuth, posts.destroy);
  // HOME PAGE
  app.get('/', posts.index);

  //===========================================================================
  // ADMIN
  //===========================================================================
  app.get('/admin', adminAuth, function (req, res) {
    res.render('admin', { user: req.user });
  });

  //===========================================================================
  // SEARCH
  //===========================================================================
  app.get('/search', posts.search);

  //===========================================================================
  // TAGS
  //===========================================================================
  app.get('/tags/:tag', tags.index);

  //===========================================================================
  // GALLERY
  //===========================================================================
  app.get('/gallery', images.index);
  app.get('/gallery/new', adminAuth, images.new);
  app.post('/gallery', imageAuth, images.handlePost);

  app.get('/gallery/:name', images.show);
  app.get('/gallery/:name/new', adminAuth, images.new);
  app.get('/gallery/:name/edit', adminAuth, images.edit);
  app.post('/gallery/:name', imageAuth, images.handlePost);
  app.del('/gallery/:name', adminAuth, images.destroy);
  app.del('/gallery/:name/:image/img', adminAuth, images.destroyImage);
  app.get('/gallery/:name/regenerate', adminAuth, images.regenerate);

  app.get('/gallery/:name/:place', images.show);
  app.get('/gallery/:name/:place/new', adminAuth, images.new);
  app.get('/gallery/:name/:place/edit', adminAuth, images.edit);
  app.post('/gallery/:name/:place', imageAuth, images.handlePost);
  app.del('/gallery/:name/:place', adminAuth, images.destroy);
  app.del('/gallery/:name/:place/:image/img', adminAuth, images.destroyImage);
  app.get('/gallery/:name/:place/regenerate', adminAuth, images.regenerate);

  app.get('/gallery/:name/:place/:part', images.show);
  app.get('/gallery/:name/:place/:part/new', adminAuth, images.new);
  app.get('/gallery/:name/:place/:part/edit', adminAuth, images.edit);
  app.post('/gallery/:name/:place/:part', imageAuth, images.handlePost);
  app.del('/gallery/:name/:place/:part', adminAuth, images.destroy);
  app.del('/gallery/:name/:place/:part/:image/img', adminAuth, images.destroyImage);
  app.get('/gallery/:name/:place/:part/regenerate', adminAuth, images.regenerate);

  // gets all images as JSON:
  app.get('/gallerylister', images.getList);

  //===========================================================================
  // BUSINESS
  //===========================================================================
  app.get('/business', business.index);
  app.get('/business/new', adminAuth, business.new);
  app.post('/business', adminAuth, business.create);
  app.get('/business/:title/edit', adminAuth, business.edit);
  app.post('/business/:title', adminAuth, business.update);
  app.del('/business/:id', adminAuth, business.destroy);

  //===========================================================================
  // SECTIONS
  //===========================================================================
  app.get('/section/:name', sections.index);
  app.get('/about', sections.about);

  //===========================================================================
  // COMMENTS
  //===========================================================================
  app.param('commentId', comments.load);
  app.post('/posts/:permalink/comments', auth.requiresLogin, comments.create);
  app.get('/posts/:permalink/comments', auth.requiresLogin, comments.create);
  app.del('/posts/:permalink/comments/:commentId', commentAuth, comments.destroy);
};