var pkg = require('../package');
var express = require('express');
var path = require('path');
var hbs = require('express-hbs');
var env = process.env.NODE_ENV || 'development';
var config = require('./config')[env];
var flash = require('connect-flash');
var security = require('../tools/securityUtil');
var url = require('url');
var qs = require('querystring');
var multer = require('multer');
var os = require('os');

var mongoose = require('mongoose');

module.exports = function (app, passport) {
  app.configure(function () {
    // all environments
    app.set('port', process.env.PORT || 3000);

    // Handlebars setup:
    app.set('views', path.join(config.root, 'views'));
    if (env === 'production') {
      app.enable('view cache');
    }
    app.engine('hbs', hbs.express3({
      partialsDir: config.root + '/views/partials',
      defaultLayout: config.root + '/views/layouts/main.hbs',
      layoutsDir: config.root + '/views/layouts'
    }));
    app.locals.layout = "main";
    app.set('view engine', 'hbs');

    hbs.registerHelper('localeDate', function(date){
      var months = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
      return date.getDate() +
          ' ' +
          months[date.getMonth()] +
          ' ' +
          date.getFullYear() +
          ' в ' +
          (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) +
          ':' +
          (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());
    });
    hbs.registerHelper('createPagination', function(pages, page) {
      if (pages <= 1)
        return '';

      var params = {};
      var str = page == 1 ? '' : '<li><a href="?page=' + (page - 1) + '">&laquo;</a></li>';
      params.page = 1;
      var clas = page == 1 ? "active" : "no";

      for (var p = 1; p <= pages; p++) {
        params.page = p;
        clas = page == p ? "active" : "no";
        var href = '?' + qs.stringify(params);
        str += '<li class="'+clas+'"><a href="'+ href +'">'+ p +'</a></li>';
      }

      str += page === pages ? '' : '<li><a href="?page=' + (page + 1) +'">&raquo;</a></li>';
      return str;
    });
    hbs.registerHelper('createPager', function(pages, page) {
      if (pages <= 1)
        return '';

      var str = page === pages ? '' : '<li class="previous"><a href="?page='+ (page + 1) +'">← Предыдущие 30 записей</a></li>';
      str += page === 1 ? '' : '<li class="next"><a href="?page='+ (page - 1) +'">Следующие 30 записей →</a></li>';

      return str;
    });
    hbs.registerHelper('isAdmin', function (user, fn, elseFn) {
      if (user && security.isAdmin(user)) {
        return fn.fn();
      } else {
        if (elseFn)
          return elseFn();
        return '';
      }
    });
    hbs.registerHelper('isWriterOrAdmin', function (user, fn, elseFn) {
      if (user && security.canUploadFiles(user)) {
        return fn.fn();
      } else {
        if (elseFn)
          return elseFn();
        return '';
      }
    });
    hbs.registerHelper('canDeleteComment', function (user, commentUser, fn, elseFn) {
      if (user && commentUser && security.canDeleteComment(user, commentUser)) {
        return fn.fn();
      } else {
        if (elseFn)
          return elseFn();
        return '';
      }
    });
    hbs.registerHelper('canReadPost', security.canReadPost);
    hbs.registerHelper('canModifyPost', security.canModifyPost);
    hbs.registerHelper('getCut', function(body) {
      return body.replace(/<cut.*<\/cut>([\r\n|\n|\r]*.*)*/gim, '');
    });
    hbs.registerHelper('trimComment', function(body){

    });
    hbs.registerHelper('getCommentLink', function(comment){
      var Post = mongoose.model('Post');

      var dotIndex = comment.body.search(/[\.\?!]/);
      dotIndex = dotIndex > 0 ? dotIndex + 1 : comment.body.length;
      dotIndex = comment.body.length > 300 ? dotIndex : comment.body.length;
      var trimmedComment = comment.body.substr(0, dotIndex);
      trimmedComment = comment.body.length > 300 ? trimmedComment + " (...)" : trimmedComment;

      var link = comment.permalink === undefined ? trimmedComment : "<a href=\"/posts/" + comment.permalink + "#" +
          comment._id + "\">" + trimmedComment + "</a>";
      return "<p><strong>" + comment.author.username + "</strong>: " + link + "</p>";
    });

    app.use(express.favicon(path.join(config.root, '/public/img/favicon.ico')));
    app.use(express.logger('dev'));
    app.use(express.json()).use(express.urlencoded());

    // bodyParser should be above methodOverride
    //app.use(express.bodyParser());
    app.use(multer({dest: os.tmpDir(), inMemory: true}).any());
    app.use(express.methodOverride());

    // cookieParser should be above session
    app.use(express.cookieParser(pkg.name));
    app.use(express.session({
      secret: pkg.name
    }));

    // Passport session
    app.use(passport.initialize());
    app.use(passport.session());

    // development only
    if ('development' == app.get('env')) {
      app.use(express.errorHandler());
    }

    if ('production' == env) {
      app.enable('trust proxy');
    }

    // CSRF protection!
    /*app.use(express.csrf()).use(function (req, res, next) {
      res.cookie('XSRF-TOKEN', req.csrfToken());
      res.locals.csrf_token = req.csrfToken();
      next();
    });*/

    // flash:
    app.use(flash());

    // routes should go last
    app.use(app.router);

    app.use(express.static(path.join(config.root, 'public')));

    // custom error handler
    app.use(function (err, req, res, next) {
      if (err.message
          && (~err.message.indexOf('not found')
          || (~err.message.indexOf('Cast to ObjectId failed')))) {
        return next();
      }

      console.error(err.stack);
      return res.status(500).render('500');
    });

    app.use(function (req, res, next) {
      res.status(404).render('404', { url: req.originalUrl })
    });
  });
};