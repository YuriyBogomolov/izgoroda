var env = process.env.NODE_ENV || 'development';

var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var VkontakteStrategy = require('passport-vkontakte').Strategy;

// load up the user model
var User = require('../models/User');
var gravatar = require('gravatar');
var config = require('./config')[env];

// expose this function to our app using module.exports
module.exports = function (passport) {
  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  //===========================================================================
  // LOCAL SIGNUP & LOGIN
  //===========================================================================
  passport.use('local-signup', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) {
      // asynchronous
      // User.findOne wont fire unless data is sent back
      process.nextTick(function () {
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        if (!req.user) {
          User.findOne({ 'email': email }, function (err, user) {
            // if there are any errors, return the error
            if (err)
              return done(err);

            // check to see if there's already a user with that email
            if (user) {
              return done(null, false, req.flash('signupMessage', 'Такой email уже занят.'));
            } else {
              // if there is no user with that email
              // create the user
              var newUser = new User();

              // set the user's local credentials
              newUser.username = req.body.username;
              newUser.email = email;
              newUser.password = password;
              newUser.avatar_url = gravatar.url(email, { s: 100, d: 'mm', r: 'pg' });

              // save the user
              newUser.save(function (err) {
                if (err)
                  throw err;
                return done(null, newUser);
              });
            }
          });
        } else {
          var user = req.user;
          user.email = email;
          user.password = password;
          user.save(function(err) {
            if (err)
              throw err;
            return done(null, user);
          });
        }
      });
    })
  );

  passport.use('local-login', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form
      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to login already exists
      User.findOne({ 'email' :  email }, function(err, user) {
        // if there are any errors, return the error before anything else
        if (err)
          return done(err);

        // if no user is found, return the message
        if (!user)
          return done(null, false, req.flash('loginMessage', 'Пользователь не найден.')); // req.flash is the way to set flashdata using connect-flash

        // if the user is found but the password is wrong
        if (!user.isValidPassword(password))
          return done(null, false, req.flash('loginMessage', 'Неправильный пароль. Проверьте и попробуйте еще раз.')); // create the loginMessage and save it to session as flashdata

        if (user.avatar_url === ''){
          user.avatar_url = gravatar.url(user.email, { s: 100, d: 'mm', r: 'pg' });
        }
        user.last_login = new Date();
        user.save();
        // all is well, return successful user
        return done(null, user);
      });
    })
  );

  //===========================================================================
  // FACEBOOK SIGNUP & LOGIN
  //===========================================================================
  passport.use(new FacebookStrategy({
      clientID: config.auth.facebookAuth.clientID,
      clientSecret: config.auth.facebookAuth.clientSecret,
      callbackURL: config.auth.facebookAuth.callbackURL,
      passReqToCallback: true
    },
    function (req, token, refreshToken, profile, done) {
      console.log(profile);
      process.nextTick(function() {
        // check if the user is already logged in
        if (!req.user) {
          User.findOne({ $or: [{'facebook.id': profile.id}, {'email': profile.emails[0].value}] }, function (err, user) {
            if (err)
              return done(err);

            if (user) {
              user.last_login = new Date();

              if (!user.facebook.token) {
                user.facebook.token = token;
                user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = profile.emails[0].value;

                user.save(function (err) {
                  if (err)
                    throw err;
                  return done(null, user);
                });
              }

              user.save(function (err) {
                if (err) throw err;
                return done(null, user);
              });
            } else {
              var newuser = new User({
                name: profile.displayName,
                email: profile.emails[0].value,
                username: profile.username,
                avatar_url: "https://graph.facebook.com/" + profile.id + "/picture",
                provider: 'facebook',
                facebook: profile._json
              });
              newuser.facebook.profileUrl = profile.profileUrl;
              newuser.save(function (err) {
                if (err)
                  console.log(err);

                return done(err, newuser);
              });
            }
          });
        } else {
          var user = req.user;

          user.facebook = {};
          user.facebook.id    = profile.id;
          user.facebook.token = token;
          user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
          user.facebook.email = profile.emails[0].value;
          user.facebook.profileUrl = profile.profileUrl;
          user.markModified('facebook');

          // save the user
          user.save(function(err) {
            if (err)
              throw err;
            return done(null, user);
          });
        }
      });
    })
  );

  //===========================================================================
  // VKONTAKTE SIGNUP & LOGIN
  //===========================================================================
  passport.use(new VkontakteStrategy({
      clientID: config.auth.vkontakteAuth.clientID,
      clientSecret: config.auth.vkontakteAuth.clientSecret,
      callbackURL: config.auth.vkontakteAuth.callbackURL,
      passReqToCallback: true
    },
    function(req, token, refreshToken, profile, done) {
      process.nextTick(function() {
        if (!req.user) {
          User.findOne({ 'vkontakte.id': profile.id }, function (err, user) {
            if (err)
              return done(err);

            if (user) {
              // This is 'login'
              user.last_login = new Date();

              user.vkontakte = profile._json;
              user.vkontakte.token = token;
              user.markModified('vkontakte');
              user.last_login = new Date();
              user.avatar_url = user.vkontakte.photo;

              user.save(function(err) {
                if (err)
                  throw err;
                return done(null, user);
              });
            } else {
              // This is 'signup'
              var newuser = new User({
                name: profile.displayName,
                //email: profile.emails[0].value, // we don't have email in VK :(
                username: profile.displayName, // profile.username
                provider: 'vkontakte',
                avatar_url: profile.photo,
                vkontakte: profile._json
              });
              newuser.vkontakte.profileUrl = profile.profileUrl;
              newuser.save(function (err) {
                if (err)
                  console.log(err);

                return done(err, newuser);
              });
            }
          });
        } else {
          // This is 'attach profile'
          var user = req.user;

          user.vkontakte = profile._json;
          user.vkontakte.id    = profile.id;
          user.vkontakte.token = token;
          user.vkontakte.username  = profile.name.givenName + ' ' + profile.name.familyName;
          user.vkontakte.name  = profile.name.givenName + ' ' + profile.name.familyName;
          user.vkontakte.profileUrl = profile.profileUrl;
          user.avatar_url = user.vkontakte.photo;
          user.last_login = new Date();

          user.markModified('vkontakte');

          // save the user
          user.save(function(err) {
            if (err)
              throw err;
            req.user = user;
            return done(null, user);
          });
        }
      });
    })
  );
};